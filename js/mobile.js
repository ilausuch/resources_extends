/*
device=new Object;
device.name="";
device.cordova="";
device.platform="Computer";
device.uuid=0;
device.version=0;
*/
Connection=new Object;
Connection.UNKNOWN="unknown";
Connection.ETHERNET="ethernet";
Connection.WIFI="wifi";
Connection.CELL_2G="2g";
Connection.CELL_3G="3g";
Connection.CELL_4G="4g";
Connection.CELL="cellular";
Connection.NONE="none";


/*
Must implement:

function mobile_deviceReady(){..} //When device is ready

Optional to implement:

function mobile_online(){...}
function mobile_offline(){...}

Variables:

- mobile.isMobile : determine if is mobile

Operations:

- beep()
- vibrate(duration in ms)
- alert(message, alertCallback)
- confirm(message, alertCallback)
- notification(message)
*/

var mobile = {
	conf_permamentInterval:60000,
	conf_permamentIntervalOn:true,
	conf_detectOrientationOn:true,
	conf_detectConnectionOn:true,
	
	onDeviceReady:function(){},
	onOnline:function(){},
	onOffline:function(){},
	onConnectionWithoutCosts:function(){},
	onConnectionWithCosts:function(){},
	onPause:function(){},
	onResume:function(){},
	onVertical:function(){},
	onHorizontal:function(){},
	onChangeConnection:function(){},
	onNotificationAdd:function(id,state,json){},
	onNotificationTrigger:function(id,state,json){},
	onNotificationClick:function(id,state,json){},
	onNotificationCancel:function(id,state,json){},
		
	start: function(){
		//Check if is mobile
		mobile.isMobile=navigator.userAgent.toLowerCase().match('mobile')=="mobile";
		mobile.connection="unknown";
		
		//Inicialize
		if (mobile.isMobile) {
			if (console) console.debug("*");
		    loadScript('cordova.js', function() {
				mobile.initialize();
			});
		    
		}
		else{
			if (console) console.log("Mode web");
			mobile.sys_mobile_deviceReady();	
		}
	},

    initialize: function() {
    	mobile.bindEvents();
    },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', mobile.sys_mobile_deviceReady, false);
    },
    
   
	beep: function(){
		if (mobile.isMobile)
			navigator.notification.beep(1);
		else
			alert("beep");
	},
	
	vibrate: function(duration){
		if (mobile.isMobile)
			navigator.notification.vibrate(duration);
		else
			alert("vibrate");
	},
	
	alert: function(message, alertCallback){
		if (mobile.isMobile)
			navigator.notification.alert(message, alertCallback);
		else
			firefoxNotification(message);
	},
	
	confirm: function(message, alertCallback){
		if (mobile.isMobile)
			navigator.notification.confirm(message, confirmCallback);	
		else
			confirm(message, confirmCallback);	
	},
	
	/**
	Allows easy and simple local notification
	Uses https://github.com/katzer/cordova-plugin-local-notifications
	*/
	notification: function (message){
		if (mobile.isMobile)
			window.plugin.notification.local.add({ message: message});
		else
			firefoxNotification(message);
	},
	
	  
	/**
		getConnectionType() get connexion type
	*/
	getConnectionType: function(){
		if (mobile.isMobile)
			return navigator.connection.type;
		else
			return "wifi";
	},
	
	/**
	getConnectionTypeString() get connection information in beautifull mode
	*/
	getConnectionTypeString: function(){
	    var networkState = mobile.getConnectionType();
	
	    var states = {};
	    states[Connection.UNKNOWN]  = 'Unknown connection';
	    states[Connection.ETHERNET] = 'Ethernet connection';
	    states[Connection.WIFI]     = 'WiFi connection';
	    states[Connection.CELL_2G]  = 'Cell 2G connection';
	    states[Connection.CELL_3G]  = 'Cell 3G connection';
	    states[Connection.CELL_4G]  = 'Cell 4G connection';
	    states[Connection.CELL]     = 'Cell generic connection';
	    states[Connection.NONE]     = 'No network connection';
	
		return states[networkState];
	},
	
	checkIfWithCosts:function(c){
		return c!=Connection.ETHERNET && c!=Connection.WIFI;
	},
	
	checkIfIsOnline: function(c){
		return c!=Connection.UNKNOWN && c!=Connection.NONE;
	},
	
	/**
		isConnectionWithCosts() returns if connections has extra costs
	*/
	isConnectionWithCosts: function (){
		var conn=mobile.getConnectionType();
		return mobile.checkIfWithCosts(conn);
	},
	
	/**
		isOnline() returns if has internet connection
	*/
	/*
	isOnline: function (){
		var conn=mobile.getConnectionType();
		return mobile.checkOnline(conn);
	},
	*/
	
	/**
		isVertical() returns true if screen is in vertical mode
	*/
	isVertical: function (){
		return window.screen.height > window.screen.width;
	},
	
	/**
		isHorizontal() returns true if screen is in horizontal mode
	*/
	isHorizontal: function (){
		return !mobile.isVertical();
	},
	
	/**
		load(xpath) load an item from localStorage
	*/
	load: function(xpath){
		return window.localStorage.getItem(xpath);	
	},
	
	/**
		save(xpath,data) save an intem data to localStorage
	*/
	save: function(xpath,data){
		window.localStorage.setItem(xpath,data);
	},
	
	
	
	sys_checkConnection: function (){
		if (!mobile.conf_detectConnectionOn)
			return;
			
		//Check connection
		var connection=mobile.getConnectionType();
		if (connection!=mobile.connection)
			mobile.onChangeConnection(connection);
		
		if (mobile.checkIfWithCosts(connection) && !mobile.checkIfWithCosts(mobile.connection))
			mobile.onConnectionWithCosts();
			
		if (!mobile.checkIfWithCosts(connection) && mobile.checkIfWithCosts(mobile.connection))
			mobile.onConnectionWithoutCosts();
		
		mobile.connection = connection;
	},
	
		
	sys_connectionTypeInterval: function (){
		if (mobile.conf_permamentIntervalOn && mobile.connectionTypeInterval==undefined)
			mobile.connectionTypeInterval = setInterval(function(){
				mobile.sys_checkConnection()
			}, mobile.conf_permamentInterval);
	},
	
	sys_mobile_pause: function (){
		mobile.onPause();
	},
	
	sys_mobile_resume: function (){
		mobile.onResume();
	},
	
	sys_onOrientationChange: function(){
		if (!mobile.isVertical()) //Inversed because event is previous to change
			mobile.onVertical();
		else
			mobile.onHorizontal();
	},
	
	sys_onOnline: function(){
		if (mobile.isOnline!=1){
			mobile.isOnline=1
			mobile.onOnline();	
			mobile.sys_checkConnection();	
		}
		
	},
	
	sys_onOffline: function(){
		if (mobile.isOnline!=0){
			mobile.isOnline=0
			mobile.onOffline();	
		}
		
	},
	
	/**
	System ready
	*/
	sys_mobile_deviceReady: function (){
		//Prepare several inits
		mobile.orientation = mobile.isVertical();
		mobile.connection = mobile.getConnectionType();
		mobile.connectionTypeInterval=undefined;
		mobile.isOnline=-1;
		
		//Send ready
		mobile.onDeviceReady();
		
		//Prepare connection timmer
		mobile.sys_connectionTypeInterval();
		
		//Send initial orientation event
		if (mobile.isVertical())
			mobile.onVertical();
		else
			mobile.onHorizontal();
		
		//Prepare events
		document.addEventListener('offline', mobile.sys_onOffline, false);
		document.addEventListener('online', mobile.sys_onOnline, false);
		document.addEventListener('pause', mobile.sys_mobile_pause, false);
		document.addEventListener('resume', mobile.sys_mobile_resume, false);
		window.addEventListener("orientationchange",mobile.sys_onOrientationChange,false);
		
		//If is not mobile
		if (!mobile.isMobile){
			// check orientation every 500 ms
			mobile.orientation=mobile.isVertical();
			setInterval(function(){
				var orientation = window.screen.height > window.screen.width;
			
				if (orientation!=mobile.orientation)
					if (orientation)
						mobile.onVertical();
					else
						mobile.onHorizontal();
						
				mobile.orientation = orientation;
			},500);
			
			//Send online event
			mobile.sys_onOnline();
		}
		
		//If is mobile
		if (mobile.isMobile){
			window.plugin.notification.local.onadd = function (id, state, json) {
				mobile.onNotificationAdd(id,state,json)
			}
			window.plugin.notification.local.ontrigger = function (id, state, json) {
				mobile.onNotificationTrigger(id,state,json)
			}
			window.plugin.notification.local.onclick = function (id, state, json) {
				mobile.onNotificationClick(id,state,json)
			}
			window.plugin.notification.local.oncancle = function (id, state, json) {
				mobile.onNotificationCancel(id,state,json)
			}
			
			StatusBar.hide();
		}
		
	},
	
	
};

function completeMobileMainController(){
	mc.viewControl={
		header:{
			title:"App"	
		},
		leftMenu:{
			title:"Menu",
			visible:true,
			items:[]
		},
		rightMenu:{
			title:"Menu right",
			visible:false,
			items:[]
		},
		leftButtons:{
			items:[]
		},
		rightButtons:{
			items:[]
		}
	};
}
