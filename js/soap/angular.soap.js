/**
	@2015 Ivan Lausuch <ilausuch@gmail.com>
	
	config is an object like this
	{
		ws: "...." 						//Is the Soap web service, (without ?WSDL)
		op: "...." 						//Is the operation
		success: function(data){....} 	//Is the operation called when success (optional)
		error: function(err){...} 		//Is the operation called when error (optional)
		forceArray: [true|false] 		//Force than any result will be an array (optinal, by default is false)
		forceOneObject: [true|false]	//Force to return one object, if result is an array function returns first element
		debug: [true|false]				//Show information in console
		objectName: "..."				//Define what is the name of objects in response structure (optional)
		objectNameDS: "..."				//Define what is the DS object name in response structure (optional, by default objectName+"DS")
		$timeout						//$timeout angular object to syncronize data changes with angular	
	}
*/
function callWS(config){
	if (config.forceArray==undefined)
		config.forceArray=false;
		
	if (config.forceOneObject==undefined)
		config.forceOneObject=false;
	
	if (config.debug==undefined)
		config.debug=false;
		
	if (config.namespace==undefined)
		config.namespace="http://tempuri.org/";
		
	if(console && config.debug) console.debug("WS","Execute",config.op);
	
	var sconfig={
		url:config.ws,
		method:config.op,
		namespaceURL:config.namespace,
		appendMethodToURL: false,
		soap12:false,
		SOAPAction: config.namespace+config.op, 
		success:function(result){
			
			callback=function(data,config){
				
				countAttrObject=function(object){
					var count = 0;
					for (var k in object)
						if (object.hasOwnProperty(k))
						   ++count;
					
					return count;
				}
				
				if (result instanceof Array){
					result.forEach(function(item){
						if (result.xmlns!=undefined)
						delete result.xmlns;
					});
				}else{
					if (result.xmlns!=undefined)
						delete result.xmlns;
				}
				
				if(console && config.debug) console.debug("WS","result previous",config.op,result);
				
				if (config.forceArray)
					if (!(result instanceof Array)){
						if (countAttrObject(result)>0)
							result=[result];
						else
							result=[];
					}
						
				if (config.forceOneObject)
					if (result instanceof Array)
						result=result[0];
						
				if (!(result instanceof Array))
					if (countAttrObject(result)==0)
						result=undefined;
				
				if(console && config.debug) console.debug("WS","result",config.op,result);
				
				if (config.success!=undefined)
					if (config.$timeout==undefined)
						config.success(result);
					else
						config.$timeout(function(){config.success(result);});
			}
			
			if(console && config.debug) console.debug("WS","result RAW",config.op,result);
			result=result.toJSON();
			
			var result=result["Body"][config.op+"Response"][config.op+"Result"];
			
			if (config.objectName!=undefined){
				if (config.objectNameDS==undefined)
					config.objectNameDS=config.objectName+"DS";
					
				if (result[config.objectNameDS]==undefined){
					callback([],config);
					return;
				}
				
				result=result[config.objectNameDS];
				
				if (result[config.objectName]==undefined){
					callback([],config);
					return;
				}
				
				result=result[config.objectName];
			}
			
			callback(result,config);
		},
		error:function(err){
			if(console && config.debug) console.debug("WS","Error",config.op,err);
			
			if (config.error)
				config.error(err)
		}
	}
	
	if (config.data!=undefined)
		sconfig.data=config.data;
	else
		sconfig.data={};
	
	if(console && config.debug) console.debug("WS","Config",config.op,sconfig);
	
	$.soap(sconfig);
}