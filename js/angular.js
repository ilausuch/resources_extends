/**
Angular application complements
author: Ivan Lausuch Sales
Started on 2014
*/

function createGeneralDirectives(app){

}

var postOptions={headers: {'Content-Type': 'application/x-www-form-urlencoded'}};

function angularPost($http,url,data){
	return $http.post(url, transforParams2Post(data), postOptions)
}

function angularDelete($http,url,data){
	//return $http.post(url, transforParams2Post({deleted:1}), postOptions)
	return $http.delete(url, transforParams2Post(data), postOptions)
}


/*
Watch Register to ensure to clean all watch listeners when change the view	
*/
function WatchRegister(){
	this.list=[];
	
	this.add=function(variable,callback){
		this.register(mc.$scope.$watch(variable, callback));
	}
	
	this.register=function(watch){
		this.list.push(watch);
	}
	
	this.clear=function(){
		for(i in this.list)
			this.list[i]();
			
		this.list=[];
	}
	
}


function completeMainController(){
	
	if (typeof mobile==='undefined')
		mc.showWaitWindow=true;
	
	/**
	Go to a new rotute path defined in router provider 
	*/
	mc.routeTo=function(path,replace){
		replace=undefinedToFalse(replace);
		
		/*
		if (noBack){
			if (mc.$ionicHistory!=undefined){
				mc.$ionicHistory.clearCache();
				mc.$ionicHistory.nextViewOptions({
					historyRoot:true
				});
			}
		}
		
		window.location.replace("#/"+path);
		*/
		
		setTimeout(function(){
			if (replace)
				window.location.replace("#/"+path);
			else
				window.location.href="#/"+path;
		}, 100);
	}
	
	
	mc.routeTo0=function(path){
		//history.go(-history.length);
		//setTimeout(function(){window.location.href="#/"+path;},1);
		setTimeout(function(){
			window.location.replace("#/"+path);
		}, 100);
	}
	
	/**
	Go back function
	*/
	mc.back=function(){
		window.history.back();
	}

	/**
	Show wait window
	*/
	mc.showWait=function(){
		if (typeof mobile==='undefined')
			mc.showWaitWindow=true;
		else
			mc.showWaitWindow=true;
			//mc.$rootScope.toggle('myOverlay', 'on');
		
	}
	
	/**
	Hide wait window
	*/
	mc.hideWait=function(){
		if (typeof mobile==='undefined')
			mc.showWaitWindow=false;
		else
			mc.showWaitWindow=false;
			//mc.$rootScope.toggle('myOverlay', 'off');
		
		mc.update();
	}
		
	/**
	Prerequisites default check 
	*/
	mc.routePrerequisitesByDefault=function(callback,$stateParams,$location){
		callback();
	}

	
	/**
	Resolve controller
	*/
	mc.resolveController=function(basePath,controller,options,$stateParams,$location) {
		mc.c=undefined;
		mc.route={
			stateParams:$stateParams,
			location:$location,
			path:$location.$$path,
			controller:controller,
			options:options
		};
		
		if (options.prerequisitesFunction==undefined){
			mc.routePrerequisitesByDefault(
				function(){
					mc.loadController(basePath,controller,options,$stateParams);
				},
				$stateParams,
				$location
			);
		}else{
			options.prerequisitesFunction(
				function(){
					mc.loadController(basePath,controller,options,$stateParams);
				},
				$stateParams,
				$location
			);	
		}
			
		
	};
	
	//Registry of controllers to prevent to reload
	mc.controllerRegistry={}
	
	//Agregation of Resolve controller
	mc.loadController=function(basePath,controller,options,$stateParams){
		
		if (mc.controllerRegistry[controller]===undefined){
			var deferred = mc.$q.defer();
			var script = document.createElement('script');
			script.src = basePath+"/"+controller+"/"+controller+".js?v="+Math.random();
			if (console) console.debug(script.src);
			script.onload = function() {
				if (console) console.info("Router","Create controller",controller+"_controller");
				mc.c=new window[controller+"_controller"]($stateParams);
				mc.controllerRegistry[controller]=mc.c;
				mc.c.$stateParams=$stateParams;
				
				if (options.onFinish!=undefined)
					options.onFinish($stateParams);
					
				if (mc.started)
					mc.startController();
				else
					mc.startSync.ready();	
				
			};
			
			document.body.appendChild(script);
			return deferred.promise;
		}
		else{
			if (options.persistent){
				mc.c=mc.controllerRegistry[controller];
				mc.c.$stateParams=$stateParams;
				
				if (console) console.info("Router","Reuse controller", controller);
				
				if (options.onFinish!=undefined)
					options.onFinish($stateParams);
					
				if (mc.c.onRestart!=undefined)
					mc.$timeout(function(){mc.c.onRestart(mc.c.$stateParams)},1);
					
				if (mc.c.onStartAndRestart!=undefined)
					mc.$timeout(function(){mc.c.onStartAndRestart(mc.c.$stateParams)},1);
					
			}else{
				if (console) console.info("Router","Create controller",controller+"_controller");
				mc.c=new window[controller+"_controller"]($stateParams);
				mc.c.$stateParams=$stateParams;
				
				if (options.onFinish!=undefined)
					options.onFinish($stateParams);
				
				if (mc.started)
					mc.startController();
				else
					mc.startSync.ready();
					
				
			}
		}
	}
	
	mc.startController=function(){
		if (mc.c.onStart!=undefined)
			mc.$timeout(function(){mc.c.onStart(mc.c.$stateParams)},1);	
			
		if (mc.c.onStartAndRestart!=undefined)
			mc.$timeout(function(){mc.c.onStartAndRestart(mc.c.$stateParams)},1);
	}
	
	mc.started=false;
	mc.startSync=new Sync(2,function(){
		mc.startController();
	});
	
	mc.start=function(){
		if (typeof mobile === 'undefined'){
			mc.started=true;
			mc.startSync.ready();
		
			if (mc.onReady!=undefined)
				mc.onReady();
		}else{
			mobile.onDeviceReady=function(){
				if (mc.onReady!=undefined){
					mc.started=true;
					mc.startSync.ready();
					mc.onReady();
				}
			}
		}			
	}
	
	mc.safeApply=function(fn) {
	   setTimeout(fn,1);
	}

	/**
	Force update of angular, in case of jquery or others events that don't interact with angular
	**/
	mc.update=function(fnc){
		if (fnc==undefined)
			fnc=function(){};

		mc.$scope.$apply(fnc);
	}
}


/**
Add a route to state route controller
*/
function stateRouteAdd($stateProvider,url,controller,options){
	stateRouteAddBase($stateProvider,url,'app',controller,options);
}

function stateRouteAddBase($stateProvider,url,basePath,controller,options){
	if (options==undefined){
		options={persistent:false}
	}else{
		options.persistent=undefinedToFalse(options.persistent);
	}
	$stateProvider
        .state(url, {
            url: url,
            templateUrl: basePath+'/'+controller+'/'+controller+'.html?v='+Math.random(),
            resolve: {
	            op:function($stateParams,$location){
	            	mc.resolveController(basePath,controller,options,$stateParams,$location)
	            }
	        }
        })	
}



/***********************************************************
	AngularTranslations
	
	Helps to use $translateProvider
	
	langs: indicate languajes to use, order is critical 
	
	var tr=new AngularTranslations(["es","fr","it"])
	tr.addMultiple({
		"Select file"=>["Seleccionar fichero","Choisissez Fichier","Scegli file"]
	}).compile(app);
	
************************************************************/	
function AngularTranslations(langs) {
	this.translations = {};
    this.langs = langs;

    for (k in this.langs){
        this.translations[this.langs[k]]={};
    }
	
    /**
        key="Selecct File";
        translations=["Seleccionar fichero","Choisissez Fichier"]
        }
   */
    this.addOne = function (key,translations) {
        for (k in translations){
            lang=this.langs[k];
            value=translations[k];

            this.translations[lang][key] = value;
        }

        return this;  
    }

    /**
        translations={
            'select File' : ['Seleccionar fichero','Choisissez Fichier']
        }
    */
    this.addMultiple = function (translations) {
        for (k in translations) 
            this.addOne(k, translations[k]);

        return this;
    }

	/**
	Compile into $translateProvider	
	*/
    this.compile = function (angular) {
        var tr=this;
        angular.config(function ($translateProvider) {
            for (lang in tr.translations) {
                $translateProvider.translations(lang, tr.translations[lang]);
            }
        });
        
        return this;
    }
}

/***********************************************************
	Angular filters
		
************************************************************/
function create_filter_mysqlDate(app){
	app.filter('filterMysqlDate', function() {
		return function(input) {
			return input ? mysql_to_date_string(input) : '';
		};
	});
	app.filter('filterMysqlDateShort', function() {
		return function(input) {
			return input ? mysql_to_date_string(input,true) : '';
		};
	});
}

function create_filter_sanitize(app){
	app.filter("sanitize", ['$sce', function($sce) {
	    return function(htmlCode){
	        return $sce.trustAsHtml(htmlCode);
	    }
	}])	
}	

function create_filter_markdown(app){
	/*
	REQUIRES: <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/showdown/1.3.0/showdown.min.js"></script>
	DOC: http://showdownjs.github.io/demo/
	USAGE: <div ng-bind-html="text|markdown"></div>
	*/
	app.filter("markdown", ['$sce', function ($sce) {
	    return function (htmlCode) {
	        var converter = new showdown.Converter();
	        return $sce.trustAsHtml(converter.makeHtml(htmlCode));
	    }
	}])
	
}
